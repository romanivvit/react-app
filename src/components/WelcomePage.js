import React from 'react';
import {Redirect} from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import LoginForm from './LoginForm';
import SignUpForm from './SingUpForm';
const styles = theme =>({
  paper: {
    marginTop: 64 + theme.spacing(3),
    width: 500,
  },
  Tabs:{
    flexGrow: 1
  },
  TabContent:{
    padding: 25,
  }
})
class ChatList extends React.Component {
  constructor(props){
    super(props);
    this.handleTabChange = this.handleTabChange.bind(this);
    this.state = { activeTab: 0}
  }

  handleTabChange = (event , value) => {
    this.setState({activeTab: value});
  };
  onSubmit = (props) =>{
    console.log('clicked submit');
  };
  render(){
    const {classes, signup, login, success} = this.props;
    const activeTab = this.state.activeTab;
    console.log(success);
    if(success){
      return(
        console.log('NOT REDIRECTED'),
        <Redirect to='/chat' />
      )
    }
    return(
     <div className={classes.rootWrapper}>
         <AppBar position="fixed" className={classes.appBar}>
          <Toolbar>
            <Typography variant="h6" color="inherit" noWrap>
              React Chat
            </Typography>
          </Toolbar>
        </AppBar>
        <Grid container justify="center" alignItems="center">
          <Grid item>
          <Paper className={classes.paper}>
            <AppBar position='static' color='default'>
              <Tabs
                value={activeTab}
                onChange={this.handleTabChange}
              >
                <Tab label="Login" className={classes.Tabs} />
                <Tab label="Sign Up" className={classes.Tabs} />
              </Tabs>
            </AppBar>
            <div className={classes.TabContent}>
              {activeTab === 0 && 
                <LoginForm onSubmit={login}/>
              }
              {activeTab === 1 && 
                <SignUpForm onSubmit={signup} />
              }
            </div>
          </Paper>
          </Grid>
        </Grid>
     </div>
    );
  }
}
export default withStyles(styles)(ChatList);
