const { createStore } = require('redux');
//action
const increment = {
  type: 'INCREMENT'
};

const decrement = {
  type: 'DECREMENT'
};

//state
const initialState = 10;


function reducer(state = initialState,action){
switch(action.type){
  case 'INCREMENT':
    return state + 1;

  case 'DECREMENT':
    return state - 1;

  default:
    break;
  }
}

const store = createStore(reducer);

const unsubscribe = store.subscribe(()=>{
  console.log(store.getState())
})

const interval = setInterval(()=>{
  store.dispatch(decrement);
  if(store.getState() <= 1){
    clearInterval(interval)
    unsubscribe();
  }
},1000);
