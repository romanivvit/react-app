import React from 'react';
import MessageInput from './MessageInput';
import ChatList from './ChatList';
import { withStyles } from '@material-ui/styles';

const styles = theme => ({
  chatLayout: {
    height: '100%',
    width: '100%',
    display: 'flex',
    overflow: 'hidden',
    alignItems: 'center',
    paddingTop:'64px',
    justifyContent: 'center'
  },
  formControl:{
    marginBottom: '8px'
  },
})
const Chat = ({classes, message}) => {
  return (
    <main className={classes.chatLayout}>
        <ChatList message={message}/>
        <MessageInput />
    </main>
  );
}

export default withStyles(styles)(Chat);
