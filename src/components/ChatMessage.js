import React from 'react';
import classNames from 'classnames';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/styles';

const styles = theme => ({
  messageFromMe:{
    maxWidth: '70%',
    minWidth: '10%',
    marginRight: '16px'
  },
  message:{
    marginLeft: '16px',
    padding: '10px'
  },
})

const ChatMessage = ({classes, isMessageFromMe,message}) => {
  return(
    <Paper className={classNames(
      classes.message,
      isMessageFromMe && classes.messageFromMe
    )}>

      <Typography variant='caption'>
        {message.sender}
      </Typography>

      <Typography variant='body1'>
        {message.content}
      </Typography>

    </Paper>
  );
}

export default withStyles(styles)(ChatMessage);
