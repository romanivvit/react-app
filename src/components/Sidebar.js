import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import RestoreIcon from '@material-ui/icons/Restore';
import ExploreIcon from '@material-ui/icons/Explore';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import titleInitials from '../utils/title-initials';
import Avatar from '@material-ui/core/Avatar';
const styles = theme => ({
  drawer: {
    width: 320,
    flexShrink: 0,
    position: 'relative'
  },
  drawerPaper: {
    width: 320,
    height: '100%'
  },
  chatsList:{
    height: `calc(100% - 140px)`,
    overflow: 'scroll'
},
  chatListItem:{
    padding: '0 15px'
  },
  searchWrapper:{
    padding: '10px 20px 10px 20px',
},
  searchField:{
    width: '100%',
},
addChat:{
  position: 'absolute',
  right: '20px',
  bottom: '90px'
},
paper: {
  position: 'absolute',
  display: 'flex',
  width: '100%',
  bottom: 0,
},
})

const Sidebar = ({ classes,chats }) => {
  const [value] = React.useState(0);

  return (
    <Drawer
    className={classes.drawer}
    variant="permanent"
    classes={{
      paper: classes.drawerPaper,
    }}
    >

      {/* Search field start */}
      <div className={classes.searchWrapper}>
        <TextField
          id="standard-search"
          label="Search chats"
          type="search"
          className={classes.searchField}
        />
      </div>
      {/* Search field end */}


      {/* Chats list start */}
      <List className={classes.chatsList}>
      {chats.map((chat,index)=>(
        <ListItem key={index} button>
          <Avatar>
          {titleInitials(chat.title)}
          </Avatar>
          <ListItemText primary={chat.title} className={classes.chatListItem} />
        </ListItem>
      ))}
      </List>
      {/* Chats list end */}

      {/* Add new chat start */}
      <Fab color="primary" aria-label="Add" className={classes.addChat}>
        <AddIcon />
      </Fab>
      {/* Add new chat end */}

      {/* Navigation start */}
      <Paper square className={classes.paper}>
        <Tabs
          value={value}
          indicatorColor="secondary"
          textColor="secondary"
        >
          <Tab icon={<RestoreIcon />} label="My Chats" />
          <Tab icon={<ExploreIcon />} label="Explore" />
        </Tabs>
      </Paper>
      {/* Navigation end */}

      <div className={classes.toolbar} />
    </Drawer>
)
};

export default withStyles(styles)(Sidebar);
