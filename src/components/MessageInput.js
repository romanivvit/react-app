import React from 'react';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import { withStyles } from '@material-ui/core';

const styles = theme =>({
  messageInputWrapper:{
    position: 'fixed',
    bottom: '10px',
    left: 'auto',
    right: '0',
    width: 'calc(100% - 360px)',
    padding: '24px'
  },
  messageInputBackground:{
    padding: '0 15px 0 15px'
  },
  messageInput:{
    width: '100%',
  },
})
class MessageInput extends React.Component {
  render(){
    const { classes } = this.props;
    return(
      <div className={classes.messageInputWrapper}>
            <Paper className={classes.messageInputBackground}>
              <TextField
                id="standard-name"
                label="Type your message..."
                className={classes.messageInput}
                margin="normal"
                />
            </Paper>
          </div>
    );
  }
}

export default withStyles(styles)(MessageInput);
