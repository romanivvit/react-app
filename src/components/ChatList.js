import React from 'react';
import classNames from 'classnames';
import Avatar from '@material-ui/core/Avatar';
import titleInitials from '../utils/title-initials';
import ChatMessage from './ChatMessage';
import { withStyles } from '@material-ui/styles';
const styles = theme =>({
  chatLayoutWrapper:{
    width: '100%',
    height: '100%',
    overflowX: 'scroll',
    paddingTop: '24px',
    paddingBottom: '120px'
  },
  messageWrapper:{
    display: 'flex',
    padding: '8px 24px',
    alignItems: 'center',
    justifyContent: 'flex-start'
  },
  messageFromMe:{
    maxWidth: '70%',
    minWidth: '10%',
    marginRight: '16px'
  },
  messageWrapperFromMe:{
    display: 'flex',
    justifyContent: 'flex-end'
  },
  message:{
    marginLeft: '16px',
    padding: '10px'
  },
})
class ChatList extends React.Component {
  render(){
    const { classes, message } = this.props;
    return(
      <div className={classes.chatLayoutWrapper}>
          {message && message.map((message,index) =>{
              const isMessageFromMe = message.sender === 'me';
              const userAvatar = (
                <Avatar>
                  {titleInitials(message.sender)}
                </Avatar>
              );

              return(
                <div
                key={index} 
                className={classNames(classes.messageWrapper, isMessageFromMe && classes.messageWrapperFromMe)
                }>

                {!isMessageFromMe && userAvatar}
                  <ChatMessage isMessageFromMe={isMessageFromMe} message={message} />
                {isMessageFromMe && userAvatar}

                </div>
              );
            })  
          }
        </div>
    );
  }
}
export default withStyles(styles)(ChatList);
