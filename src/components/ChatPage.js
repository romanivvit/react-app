import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { chats, message } from '../mock-data';
import Sidebar from './Sidebar';
import ChatHeader from './ChatHeader';
import Chat from './Chat';
const styles = theme => ({
  root: {
    display: 'flex',
    width: '100%',
    height: '100%',
    backgroundColor: theme.palette.background.paper,
  },
  content: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing * 3,
  },
});

const ChatPage = (props) => {
  const { classes } = props;
  return(
    <div className={classes.root}>
      <h1>sd</h1>
      <ChatHeader />
      <Sidebar chats={chats} />
      <Chat message={message} />
    </div>
  );
}

export default withStyles(styles)(ChatPage);
