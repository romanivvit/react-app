import React from 'react';
import TextField from '@material-ui/core/TextField';
import {withStyles} from '@material-ui/core';
import Button from '@material-ui/core/Button';
import {Redirect} from 'react-router-dom';

const styles = theme => ({
  formWrapper: {
    display: 'flex',
    flexDirection: 'column'
  },
  margin: {
    margin: theme.spacing(1)
  }
})

class LoginForm extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {
      username: '',
      password: ''
    }
  }

  handleChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  };

  handleSubmit = (event) => {
    const {
      onSubmit = () => {
      }
    } = this.props;
    event.preventDefault();
    const {username, password} = this.state;

    onSubmit(username, password);
  };

  render() {
    const ColorButton = withStyles(theme => ({
      root: {
        marginTop: '10px'
      },
    }))(Button);
    const classes = this.props;
    return (
      <form className={classes.formWrapper} autoComplete="off" onSubmit={this.handleSubmit}>
        <TextField
          required
          value={this.state.name}
          onChange={this.handleChange}
          type='name'
          name='username'
          label="Username"
          autoComplete="username"
          margin="normal"
        />
        <TextField
          required
          value={this.state.password}
          onChange={this.handleChange}
          label="Password"
          type="password"
          name='password'
          autoComplete="current-password"
          margin="normal"
        />
        <ColorButton variant="contained" color="primary" type='submit' className={classes.margin}>
          Login
        </ColorButton>
      </form>
    );
  }
}

export default withStyles(styles)(LoginForm);
