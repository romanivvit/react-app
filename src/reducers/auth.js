import {
  SIGNUP_REQUEST, SIGNUP_SUCCESS, SIGNUP_FAILURE,
  LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_FAILURE,
  LOGOUT_REQUEST, LOGOUT_SUCCESS, LOGOUT_FAILURE
} from "../constants";

const initialState = {
  success: false,
  user: null,
  token: {},
};

export default function auth(state = initialState, action) {
  switch (action.type) {
    case SIGNUP_SUCCESS:
    case LOGIN_SUCCESS:
      return {
        ...state,
        success: true,
        user: action.payload.user,
        token: action.payload.token

      };

    case LOGIN_FAILURE:
    case SIGNUP_FAILURE:
    case LOGOUT_SUCCESS:
      return {
        ...state,
        success: false,
        user: null,

      };
    default:
      return state;
  }

}
