import { connect } from 'react-redux';
import WelcomePage from '../components/WelcomePage';
import {signup, login} from '../actions';
import {bindActionCreators} from "redux";

const mapStateToProps = state => (
  {
    success: state.auth.success
  });

const mapDispatchToProps = dispatch =>bindActionCreators({
  signup,
  login
}, dispatch);

export default  connect(
  mapStateToProps,
  mapDispatchToProps
)(WelcomePage);
